// example taken from https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#Moving_the_pen

import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.math.PI

fun main(args: Array<String>) {
    val canvas = document.createElement("canvas") as HTMLCanvasElement
    document.body!!.appendChild(canvas)
    val context2D = canvas.getContext("2d") as CanvasRenderingContext2D

    context2D.fillStyle = "white"
    context2D.strokeStyle = "blue"
    context2D.beginPath()
    context2D.arc(75.0, 75.0, 50.0, 0.0, PI * 2, true) // Outer circle
    context2D.fill()
    context2D.stroke()

    context2D.beginPath()
    context2D.moveTo(110.0, 75.0)
    context2D.arc(75.0, 75.0, 35.0, 0.0, PI, false)  // Mouth (clockwise)
    context2D.moveTo(65.0, 65.0)
    context2D.arc(60.0, 65.0, 5.0, 0.0, PI * 2, true)  // Left eye
    context2D.moveTo(95.0, 65.0)
    context2D.arc(90.0, 65.0, 5.0, 0.0, PI * 2, true)  // Right eye
    context2D.stroke()
}