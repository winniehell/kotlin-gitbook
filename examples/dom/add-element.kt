import kotlin.browser.document

fun main(args: Array<String>) {
    val text = document.createElement("strong")
    text.innerHTML = "This is bold"
    document.body!!.appendChild(text)
}