#!/usr/bin/env node

const crypto = require('crypto')
const fs = require('fs')
const path = require('path')

const kotlinCompiler = require('@jetbrains/kotlinc-js-api')

function copyFile (source, destination) {
  fs.writeFileSync(destination, fs.readFileSync(source))
}

function patchBodyElement (fileName, sourceCode) {
  return sourceCode.replace(
    /document\.body(\W)/g,
    `document.getElementById("example-result")$1`
  )
}

function runSequential (promises) {
  let promise = Promise.resolve()
  promises.forEach(nextFunction => {
    promise = promise.then(nextFunction)
  })
  return promise
}

const cliArguments = process.argv.splice(2)
if (cliArguments.length !== 2) {
  console.log(`Usage: ${path.basename(__filename)} INPUT_PATH OUTPUT_PATH\n`)
  process.exit(1)
}

const [inputPath, outputPath] = cliArguments
const manifest = require(path.resolve(inputPath, 'manifest.json'))

if (!fs.existsSync(outputPath)) {
  console.log(`Creating directory ${outputPath} ...`)
  fs.mkdirSync(outputPath)
}

copyFile('README.md', path.resolve(outputPath, 'README.md'))

const categories = Object.keys(manifest).filter(name => name !== 'kotlinCompilerOptions')

runSequential(
  categories
    .map(category => () => {
      const categoryDirectory = path.resolve(outputPath, category)
      if (!fs.existsSync(categoryDirectory)) {
        console.log(`Creating directory ${categoryDirectory} ...`)
        fs.mkdirSync(categoryDirectory)
      }

      const items = manifest[category]
      return runSequential(
        items.map(item => () => {
          const fileName = path.join(category, item.entry)
          console.log(`Compiling ${fileName} ...`)

          const sourceCode = fs.readFileSync(path.resolve(inputPath, fileName)).toString()
          
          item.markdownFile = `${fileName}.md`
          fs.writeFileSync(path.resolve(outputPath, item.markdownFile), `# ${item.title}

<blockquote id="example-result"></blockquote>

<script src="../kotlin.js"></script>
<script src="../${fileName}.js"></script>

\`\`\`kotlin
${sourceCode}
\`\`\`
`)

          fs.writeFileSync(path.resolve(outputPath, fileName), patchBodyElement(fileName, sourceCode))

          const compilerOptions = {
            sourceMaps: true,
            ...manifest.kotlinCompilerOptions
          }
          compilerOptions.sources = (compilerOptions.sources || []).concat(path.resolve(outputPath, fileName))
          compilerOptions.output = path.resolve(outputPath, `${fileName}.js`)

          const hashFile = `${compilerOptions.output}.sha256`
          const sha256 = crypto.createHash('sha256')
          const hash = sha256.update(sourceCode).digest('hex')
          if (fs.existsSync(hashFile)) {
            const lastHash = fs.readFileSync(hashFile).toString()
            if (lastHash === hash) {
              console.log(`${fileName} has not changed`)
              return Promise.resolve()
            }
          }

          return kotlinCompiler
            .compile(compilerOptions)
            .then(() => {
              fs.writeFileSync(hashFile, hash)
            })
        })
      )
    })
)
  .then(() => {
    console.log('Generating SUMMARY.md ...')

    const summary = ['']

    categories
      .forEach(category => {
        summary.push(`## ${category}`)

        const items = manifest[category]
        items.forEach(item => {
          summary.push(`* [${item.title}](${item.markdownFile})`)
        })

        summary.push('')
      })

    const summaryPath = path.resolve(outputPath, 'SUMMARY.md')
    copyFile('SUMMARY.md', summaryPath)
    fs.appendFileSync(summaryPath, summary.join('\n'))
  })
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
