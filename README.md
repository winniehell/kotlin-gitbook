# kotlin-gitbook

Example [GitBook] with self-executing [Kotlin] code snippets.

[GitBook]: https://www.gitbook.com/
[Kotlin]: https://kotlinlang.org/