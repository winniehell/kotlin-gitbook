#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const { spawnSync } = require('child_process')

function findPackagePath (packageName) {
  const parentPath = require.resolve.paths(packageName).find(lookupPath => fs.existsSync(path.resolve(lookupPath, packageName)))
  return path.resolve(parentPath, packageName)
}

const cliArguments = process.argv.splice(2)
if (cliArguments.length !== 1) {
  console.log(`Usage: ${path.basename(__filename)} OUTPUT_PATH\n`)
  process.exit(1)
}

const [outputPath] = cliArguments

if (!fs.existsSync(outputPath)) {
  console.log(`Creating directory ${outputPath} ...`)
  fs.mkdirSync(outputPath)
}

const inputPath = path.resolve(findPackagePath('kotlin-compiler'), 'lib', 'kotlin-stdlib-js.jar')
console.log(`Extracting ${inputPath} to ${outputPath} ...`)
const { status } = spawnSync('unzip', ['-n', inputPath, '-d', outputPath], { stdio: 'inherit' })
if (status !== 0) {
  process.exit(status)
}
